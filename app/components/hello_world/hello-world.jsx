import React from 'react';

class HelloWorld extends React.Component {
  constructor(props) {
    super(props);
  }
  render() {
    return <h1>Hello, world! My name is {this.props.name}</h1>
  }
}

HelloWorld.defaultProps = {
  name: "@ilrein"
}

HelloWorld.propTypes = {
  name: React.PropTypes.string.isRequired
}

export default HelloWorld;