import React from 'react'

class TextArea extends React.Component {
  constructor(props) {
    super(props);
    this.state = { text: "hello from @merlo!" };
    this.recalculate = this.recalculate.bind(this);
  }
  recalculate(e) {
    this.setState({
      text: e.target.value
    });
  }
  render() {
    return (
      <div>
        <textarea onChange={this.recalculate} defaultValue={this.state.text}></textarea>
        <h3>{this.state.text.length}</h3>
      </div>
    )
  }
}

TextArea.propTypes = {
  text: React.PropTypes.string
}


export default TextArea;