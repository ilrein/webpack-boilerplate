import React from 'react';
import ReactDOM from 'react-dom';
import HelloWorld from './components/hello_world/hello-world.jsx';
import TextArea from './components/text_area/text-area.jsx';

ReactDOM.render(
  <TextArea />,
  document.getElementById('app')
)
